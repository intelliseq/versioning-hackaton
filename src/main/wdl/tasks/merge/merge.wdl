workflow merge_workflow {

  call merge

}

task merge {

  String task_name = "merge"
  String task_version = "1.0.0"
  String docker_image = "ubuntu:18.04"
  Array[File] files

  command <<<
    echo "minimal" > out
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File stdout_log = stdout()
    File stderr_log = stderr()
    File out = "out"

  }

}
