workflow minimal_workflow {

  call minimal

}

task minimal {

  String task_name = "minimal"
  String task_version = "1.0.0"
  String docker_image = "ubuntu:18.04"

  command <<<
    echo "minimal" > out
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File stdout_log = stdout()
    File stderr_log = stderr()
    File out = "out"

  }

}
