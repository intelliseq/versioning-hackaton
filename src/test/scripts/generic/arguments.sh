POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -h|--help)
    testhelp
    exit 0
    shift # past argument
    ;;
    -v|--verbose)
    export VERBOSE=TRUE
    shift # past argument
    ;;
    -i|--input-file)
    export INPUT_FILE="$2"
    shift
    shift
    ;;
    -a|--aws)
    export AWS=TRUE
    shift # past argument
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
