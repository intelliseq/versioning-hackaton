function testhelp() {
  printf "  \n"

  HEADER="$TEST_NAME ($VERSION) task tests"
  printf "  $HEADER \n"
  LEN=${#HEADER}
  printf "  "
  printf '%*s' "$LEN" | tr ' ' "*"
  printf "\n\n"

  printf "  Info:\n"
  printf "  -----\n"
  printf "    Last edited: $LAST_EDITED\n"
  printf "    Last edited by: $LAST_EDITED_BY\n"
  printf "    Author(s):\n"
  printf "$AUTHORS\n"
  printf "    Maintainer(s):\n"
  printf "      + $MAINTAINER\n"
  printf "    Copyright: Copyright 2019 Intelliseq\n"
  printf "    License: All rights reserved\n"
  printf "  \n"
  printf "  Description:\n"
  printf "  ------------\n"
  printf "  \n"
  printf "  Run $TEST_NAME tests.\n"
  printf "  \n"
  printf "      -h|--help                      print help\n"
  printf "      -v|--verbose                   print 'docker build' and 'docker push' logs\n"
  printf "      -a|--aws                       run test on aws\n"
  printf "      -i|--inputs                    file with inputs realtive to test.sh\n"
  printf "  \n"
}

export -f testhelp
