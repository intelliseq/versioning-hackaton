workflow task_name_TAG_workflow {

  call task_name_TAG

}

task task_name_TAG {

  String task_name = "task_name_TAG"
  String task_version = "latest"
  String docker_image = "ubuntu:18.04"

  command <<<
    echo "minimal" > out
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File stdout_log = stdout()
    File stderr_log = stderr()
    File out = "out"

  }

}
