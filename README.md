# versioning-hackaton
Tworzymy pipeline, który składa się z dwóch modułów i task mergującego output z tych dwóch modułów.  
Każdy z dwóch modułów składa się z trzech tasków generujących output i taska mergującego outputy.  
Każdy task generuje literkę.  
TaskA w wersji 1.0.0 generuje literkę i wersję File out = "a-1.0.0"  
TaskA w wersji 1.0.1 generuje literkę i wersję File out = "a-1.0.1"  
TaskA w wersji 1.0.2 generuje literkę i wersję File out = "a-1.0.2"  
TaskB w wersji 1.0.0 generuje literkę i wersję File out = "b-1.0.0"  
  
Wynik działania modułu Module1: "mod1-1.0.0_a-1.0.0_b-1.0.0_c-1.0.0"  
Wynik działania pieline'u Pipe: "pipe-1.0.0_mod1-1.0.0_a-1.0.0_b-1.0.0_c-1.0.0_mod2-1.0.0-d-1.0.0_e-1.0.0_f-1.0.0"  
  
Dockerfile ubuntu 18.04  
Pracujemy na gałęzi dev  
  
Wersjonujemy Taski, moduły i pipe tagami na gicie  
Importujemy na podstawie taga  
Zmiana w tasku powinna wywołać inkrementację wersji modułu  
Zmiana w module powinna wywołać inkrementację wersji pipelineu  
  
Zadania dla osób:  
Marcin Piechota:  
Utworzenie pipeline'u Pipe w wersji 1.0.0  
Utworzenie modułu Module1 w wersji 1.0.0  
Utworzenie modułu Module2 w wersji 1.0.0  
Utworzenie tasku mergującego Merge w wersji 1.0.0  
Wpięcie taska do modułu1  
Wpięcie taska do modułu2  
Wpięcie taska do pipe  
  
Monika  
Utworzenie taska TaskA w wersji 1.0.0  
Podpięcie taska TaskA do Module1  
Utworzenie taska TaskB w wersji 1.0.0  
Podpięcie taska TaskB do Module1  
Inkrementacja TaskC  
Inkrementacja TaskE  
  
Wojtek  
Utworzenie taska TaskC w wersji 1.0.0  
Podpięcie taska TaskC do Module1  
Utworzenie taska TaskD w wersji 1.0.0  
Podpięcie taska TaskD do Module2  
Inkrementacja TaskA  
Inkrementacja TaskF  
  
Mateusz i Olaf  
Utworzenie taska TaskE w wersji 1.0.0  
Podpięcie taska TaskE do Module2  
Utworzenie taska TaskF w wersji 1.0.0  
Podpięcie taska TaskF do Module2  
Inkrementacja TaskB  
Inkrementacja TaskD  
